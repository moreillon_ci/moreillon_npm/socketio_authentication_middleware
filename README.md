# Socket.io authentication middleware
A simple middleware to authenticate socket.io clients

## Usage

```javascript
const express = require('express')
const http = require('http')
const auth = require('@moreillon/socketio_authentication_middleware')
const { Server } = require("socket.io")

const port = 7000

const socketio_options = { cors: { origin: '*' } }

const app = express();
const server = http.createServer(app);
const io = new Server(server, socketio_options);


const auth_options = { url: 'https://api.authentication.example.com/v2/whoami' }
io.use( auth(auth_options))

io.on('connection', (socket) => { console.log('a user connected') })

server.listen(port, () => { console.log(`App listening on port ${port}`) })

```
