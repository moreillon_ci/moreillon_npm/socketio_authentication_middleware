const axios = require('axios')

module.exports = (options) => {
  return (socket, next) => {

    const jwt = socket.handshake.auth.token || socket.handshake.auth.jwt
    if(!jwt) return next(new Error("JWT not found"))

    const {url} = options
    const headers = { 'Authorization': `Bearer ${jwt}` }

    axios.get(url, {headers})
    .then( ({data}) => {
      socket.user = data
      next()
    })
    .catch( error => { next(error) })

  }
}
